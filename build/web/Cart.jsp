<%@ page import="p1.Db,java.util.*,p1.Product" %>
<html>
    <head>
        <title>E-Shop Items</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-5">
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>         
    <body>
             
            <jsp:include page="Header.jsp"/>
            <jsp:include page="MenuBar.jsp"/>
            <div id="container">
                
                <div id ="center" class="column">                   
                    <div id="content">
                     <%
                         p1.Shopping sp = (p1.Shopping)session.getAttribute("Cart");
                         Db obj = new Db();
                        //String val =request.getParameter("v1");
                         int min = Integer.parseInt(request.getParameter("v2"));
                         int max = Integer.parseInt(request.getParameter("v3"));
                         
                         
                     %>
                      <h1>Shopping Cart</h1>                
                         <div class="stuff">
                            
                            <%                                                                                  
                                 ArrayList arr = sp.getList();
                                 Product pr = null;
                                 int i=0;
                                 
                                for(i=min-1; i!=max;i++)
                                {            
                                     try
                                     {
                                        pr = (Product) arr.get(i);                                                                    
                                     }
                                     catch(Exception e)
                                     {
                                            break;
                                     }                                                                          
                                     %>
                                     <div class="item">
                                         <img src=<%=pr.getImage()%> alt="" width="124" height="90" /> 
                                         <a href="Details.jsp?v1=<%=pr.getName()%>" class="name"><%=pr.getName().concat(" ( x").concat(String.valueOf(pr.getQnt())).concat(")")%></a>
                                         <span>Rs <%=pr.getPrice()%></span>
                                         <a href="Details.jsp?v1=<%=pr.getName()%>"><img src="images/zoom.gif" alt="" width="53" height="19" /></a>
                                         <%
                                                    if(session.getAttribute("login")!=null)
                                                    {
                                                        %>
                                                        <a href="Remove.jsp?v1=<%=pr.getName()%>">
                                                        <img src="images/" alt="Remove" width="71" height="19" />
                                                         </a>
                                                         <%
                                                    }
                                          %>
                                      </div> 
                                      <%        
                                 }
                                 obj.close();
                             %>
                         </div>                      
                    </div>                        
                    <%
                    if(true)
                    {
                      %>               
                       <div align="right" >
                            <a href="Cart.jsp?v2=<%=max+1%>&v3=<%=max+10%>"><b><span>next>></span></b></a>
                       </div>
                       <%
                    }
                    %>
                </div>
                <jsp:include page="Categories.jsp"/>
                <!--Login table and news-->
                 <jsp:include page="right.jsp"/>
                 <jsp:include page="Footer.jsp"/>
        </div>  
    </body>
</html>
 	  