package p1;

import java.sql.*;
import javax.swing.*;
import java.util.*;

public class Db
{
    Connection con;
    public boolean ismore;
    
    
    public Db()
    {
        ismore=false;
        String path="";
        try
        {
        //path = System.getProperty("mydir");
        //Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        //String mydb ="jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ="+path+"\\Database.mdb";
        //con = DriverManager.getConnection(mydb);
         Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        con = DriverManager.getConnection("jdbc:odbc:shop");
        }
        catch(Exception e)
        {
              JOptionPane.showMessageDialog(null, path+e.toString());
        }
    }
    public boolean updateKey(String email)
    {
        boolean bool = false;
        try
        {
            Statement stmt = con.createStatement(
                                     ResultSet.TYPE_SCROLL_INSENSITIVE,
                                      ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery("select * from MyUsers");
            //PreparedStatement ps = con.prepareStatement("select * from MyUsers where param=? ");
            //ResultSet rs = ps.executeQuery();
            boolean flag = true;
            if(rs.next())
            {
                while(!rs.getString(3).equals(email))
                {
                    if(rs.isLast())
                    {
                        flag = false;
                        break;
                    }
                    else
                    {
                        rs.next();
                    }

                }
                if(flag)
                {
                    Pass p = new Pass();
                    String enc = p.encrypt(email);
                    rs.updateString(8,enc);
                    rs.updateRow();
                    MailClient obj = new MailClient();
                    obj.send(email,
                            "EShop New password link",
                            "click here to change your password http://localhost:8084/Shop/Newpass.jsp?pr=".concat(enc)
                             );
                    bool= true;
                }
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.toString());
             JOptionPane.showMessageDialog(null,"fail key update");
        }
        
       return bool;
    }
    public boolean checkKey(String key)
    {
        boolean bool =false;
        try
        {
            Statement stmt = con.createStatement(
                                     ResultSet.TYPE_SCROLL_INSENSITIVE,
                                      ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery("select * from MyUsers");
            //PreparedStatement ps = con.prepareStatement("select * from MyUsers where param=? ");
            //ResultSet rs = ps.executeQuery();
            boolean flag = true;
            if(rs.next())
            {
                while(!rs.getString(8).equals(key))
                {
                    if(rs.isLast())
                    {
                        flag = false;
                        break;
                    }
                    else
                    {
                        rs.next();
                    }

                }
                if(flag)
                {                    
                    bool= true;
                }
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.toString());
             JOptionPane.showMessageDialog(null,"fail check key");
        }
        return bool;
    }
    public boolean updatePassword(String key, String newpass)
    {
        boolean bool =false;
        try
        {
            Statement stmt = con.createStatement(
                                     ResultSet.TYPE_SCROLL_INSENSITIVE,
                                      ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery("select * from MyUsers");
            //PreparedStatement ps = con.prepareStatement("select * from MyUsers where param=? ");
            //ResultSet rs = ps.executeQuery();
            boolean flag = true;
            if(rs.next())
            {
                while(!rs.getString(8).equals(key))
                {
                    if(rs.isLast())
                    {
                        flag = false;
                        break;
                    }
                    else
                    {
                        rs.next();
                    }

                }
                if(flag)
                {
                    Pass p = new Pass();
                    rs.updateString(2,p.encrypt(newpass));
                    rs.updateString(8,"done");
                    rs.updateRow();
                    bool= true;
                }
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.toString());
             JOptionPane.showMessageDialog(null,"fail password update");
        }
        return bool;
    }



    public boolean activate(String key)
    {
        boolean result = false;
        try
        {
            Statement stmt = con.createStatement(
                                     ResultSet.TYPE_SCROLL_INSENSITIVE,
                                      ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = stmt.executeQuery("select * from MyUsers");
            


            //PreparedStatement ps = con.prepareStatement("select * from MyUsers where param=? ");
            
            //ResultSet rs = ps.executeQuery();
            boolean flag = true;
            if(rs.next())
            {
                while(!rs.getString(8).equals(key))
                {
                    if(rs.isLast())
                    {
                        flag = false;
                        break;
                    }
                    else
                    {
                        rs.next();
                    }
                                        
                }
                if(flag)
                {
                    rs.updateString(7,"yes");
                    rs.updateString(8,"done");
                    rs.updateRow();
                    result = true;
                }
                            
        
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.toString());
             JOptionPane.showMessageDialog(null,"fail");
        }
        return result;
    }
    public boolean isActivated(String username)
    {
        boolean var = false;
        String str =null;
       try
        {
            PreparedStatement ps = con.prepareStatement("select * from MyUsers where Userid=? ");
            ps.setString(1,username);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                 str = rs.getString(7);
                 if(str.equals("yes"))
                {
                    var = true;
                }


            }
            /*while(rs.next())
            {
                result = true;
                JOptionPane.showMessageDialog(null,pass);
            }*/
           
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.toString());
             JOptionPane.showMessageDialog(null,"fail8");
        }
        return var;
    }

    public boolean login(String username, String password)
    {
        boolean result = false;
        String pass =null;
        try
        {
            PreparedStatement ps = con.prepareStatement("select * from MyUsers where Userid=? ");
            ps.setString(1,username);            
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
                pass = rs.getString(2);
            }
            /*while(rs.next())
            {
                result =     true;
                JOptionPane.showMessageDialog(null,pass);
            }*/           
           if(new Pass().encrypt(password).equals(pass) & isActivated(username))
           {              
               result = true;
           }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.toString());
             JOptionPane.showMessageDialog(null,"fail");
        }
        
        return result;
    }
    
    public boolean register(String str[])
    {
        boolean result = false;
        Pass ob = new Pass();       
        str[1] = ob.encrypt(str[1]);
        str[7] = ob.encrypt(str[0]);
        try
        {
            PreparedStatement ps = con.prepareStatement("insert into MyUsers values(?,?,?,?,?,?,?,?)");
            ps.setString(1,str[0]);
            ps.setString(2,str[1]);
            ps.setString(3,str[2]);
            ps.setString(4,str[3]);
            ps.setString(5,str[4]);
            ps.setString(6,str[5]);
            ps.setString(7,str[6]);
            ps.setString(8,str[7]);

            int rows = ps.executeUpdate();
            if(rows>0)
            {
                result = true;
                MailClient obj = new MailClient();
                obj.send(str[2],"Welcome to EShop", "click here to confirm the registration. http://localhost:8080/Shop/Activate.jsp?pr=".concat(str[7]));
            }           
            
        }
       
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.toString());            
        }
             
        return result;
    }
    
    public int error(String str[])
    {
        
        try
        {
            PreparedStatement ps = con.prepareStatement("select * from MyUsers where Userid=? ");
            PreparedStatement ps1 = con.prepareStatement("select * from MyUsers where UserEmail=? ");
            ps.setString(1,str[0]);
            ps1.setString(1, str[2]);
            ResultSet rs = ps.executeQuery();
            ResultSet rs1 = ps1.executeQuery();
            if(rs.next())   
            {
                 return 1;  //when name already exist
            }
             if(rs1.next())
             {
                 return 3;  //when email already exist
             }
             int at=0,dot=0;
             if((str[2].charAt(0)<'A'||str[2].charAt(0)>'Z')||(str[2].charAt(0)<'a'||str[2].charAt(0)>'z'))
             {
                 return 0;
             }
             for(int i=0;i<str[2].length();i++)
             {
                 if(str[2].charAt(i)=='@')
                 {
                     at++;
                 }
                 if(str[2].charAt(i)=='.')
                 {
                     dot++;
                 }
             }
             if(!((dot==1 || dot==2 )&&at==1))
             {
                return 0;      //if improper email
             }
             
             
        }catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,e.toString());
        }
        return 0;   // no error
    }
  
    
     public ArrayList getAllNames()   //from category
    {
        ArrayList arr = new ArrayList();
        try
        {
            PreparedStatement ps = con.prepareStatement("select CategoryID,CategoryName from Category ");            
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                arr.add(rs.getString(1));
                arr.add(rs.getString(2));                
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, e.toString());
        }
          
       return arr;
    }
    
    public ArrayList getNames(String id)  // all product name of a category id
    {
        ArrayList arr = new ArrayList();
        try
        {
            PreparedStatement ps = con.prepareStatement("select Name from Product where CategoryID=?");            
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                arr.add(rs.getString(1));                             
            }
            
            
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,e.toString());
        }
        return arr;
    
    }
    
    public ArrayList getNameOfRange(String id, int min, int max)
    {
        ArrayList arr = new ArrayList();
        int h=min;
        try
        {
            PreparedStatement ps = con.prepareStatement("select Name from Product where CategoryID=?", ResultSet.TYPE_SCROLL_INSENSITIVE,	
                                                                                                                 ResultSet.CONCUR_UPDATABLE);            
            ps.setString(1, id);            
            ResultSet rs = ps.executeQuery();            
            if(rs.next()==false)
                return arr;
            rs.absolute(min);                         
                do
                {   
                    arr.add(rs.getString(1));  
                    h++;
                } while(rs.next()&&h<=max);               
                
                if(rs.isAfterLast()==false)
                {                   
                    ismore=true;
                }
                
           
        }
        catch(Exception e)
        {
             JOptionPane.showMessageDialog(null,e.toString());
        }
        return arr;
    }
    
    public String getCurrentCategory (String id)  //category name
    {        
        String name="";
        try
        {
            PreparedStatement ps = con.prepareStatement("select CategoryName from Category where CategoryID=?");
            ps.setString(1,id);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                name = rs.getString(1);                
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,e.toString());
        }
        
        return name;
    }    
    
    public Product getProduct(String name)
    {
        Product p =null;
      
        try
        {
            PreparedStatement ps = con.prepareStatement("select * from Product where name=?");
            ps.setString(1,name);
            ResultSet rs = ps.executeQuery();
            if(rs.next())
            {
              return new Product(rs.getString(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getString(5),rs.getString(6));                  
            }
            
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,e.toString());
        }        
        return null;
    }
    
    public ArrayList getSimilarProduct(String type)
    {
        ArrayList arr = new ArrayList();
        try
        {
            PreparedStatement ps = con.prepareStatement("select name from Product where type=?");
            ps.setString(1,type);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                arr.add(rs.getString("name"));
            }
        }
        
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,e.toString());
        }
        return arr;
    }
    
       
    public void close()
    {
        try
        {
            con.close();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, e.toString());
        }
    }
    
}
