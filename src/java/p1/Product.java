package p1;

import p1.Db;
import java.sql.*;
import javax.swing.*;
import java.util.*;


public class Product
{   
    private String catid;
    private String name;
    private int qnt;
    private String desc;
    private double price;
    private String image;
    private String type;
    private String []features;

    public static int choice;



    
    public Product(String catid, String name, String desc, double price, String image,String type)
    {
        choice = 0;
        this.setCatid(catid);
        this.setName(name);
        this.setQnt(1);
        this.setDesc(desc);
        this.setPrice(price);       
        this.setImage(image);
        this.setType(type);

        StringTokenizer st = new StringTokenizer(desc,"#");
        features = new String[st.countTokens()];
        int i=0;
        while (st.hasMoreTokens())
        {
             features[i] = new String(st.nextToken());
             i++;
        }   
    }


    public String getInterPrice(int choice)
    {
        double rs = getPrice();
        String price = null;
        if(choice == 0)
        {
            price = String.valueOf(rs);
            price = "Rs ".concat(price);
        }
        else if(choice ==1)
        {

        }

        return price;
    }
    
    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }
    
      public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQnt() {
        return qnt;
    }

    public void setQnt(int qnt) {
        this.qnt = qnt;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public String[] getFeatures()
    {
        return features;
    }
    
    
  
}