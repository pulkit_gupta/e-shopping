package p1;

import javax.swing.*;
import java.util.*;




public class Shopping
{
    public int shopcart;
    private double amt;
    ArrayList arr;
    public Shopping()
    {
        shopcart=0;
        amt=0;
        arr = new ArrayList();            
    }
    
    public String getAmt()
    {
        return String.valueOf(amt);
    }
    
    public ArrayList getList()
    {
        return arr;
    }

    public boolean removeProduct(String name)
    {      
        boolean result=false;
        Product p2 = null;
        for(int i=0;i<arr.size();i++)
        {
            try
            {
               p2 = (Product) arr.get(i);
            }
            catch(Exception e)
            {
                JOptionPane.showMessageDialog(null,"Error on Shoppingcart");
            }
            if(p2.getName().equals(name))
            {
               p2.setQnt(p2.getQnt() - 1);
               amt = amt -p2.getPrice();
               if(p2.getQnt()==0)
               {
                 arr.remove(p2);
                shopcart--;
               }
               
            }
        }
        return result;
    }
    
    public boolean addProduct(Product p)
    {
         int flag=0;
         boolean result=false;
         amt = amt + p.getPrice();
        for(int i=0;i!=arr.size();i++)
        {                       
            Product p2 = (Product) arr.get(i);
            if(p2.getName().equals(p.getName()))
            {
               p2.setQnt(p2.getQnt() + 1);           
              // JOptionPane.showMessageDialog(null,"not added incremented");
               flag=1;
               //shopcart++;
            }
            
        }
         if(flag==0)
            {
                result = arr.add(p);
                //JOptionPane.showMessageDialog(null,"added in arraylist");
                shopcart++;
            }               
        return result;                
    }
    
    
    
    
}
