
<html>     
    <head>
        <title>E-Shop</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-5">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
    </head>
    <body>
            <%
                java.io.File file = new java.io.File(getServletContext().getRealPath(request.getContextPath()));
                System.setProperty("mydir",file.getParent());                
            %>
            <jsp:include page="Header.jsp"/>
            <jsp:include page="MenuBar.jsp"/>
            <div id="container">
                <div id="center" class="column">
                    <div class="banner"><img src="images/bigbanner.jpg" alt="" width="572" height="236" /><br />
                    <div id="content">
                        <img src="images/title2.gif" alt="" width="540" height="29" /><br />
                        <p>My site is at http://www.pulkit.co.in and I am using eshop version 3.

We are setting up an ecommerce site for e-cigs and I am not having any luck with a few items.

#1) Everything is in pounds. I am in the USA, and we are only shipping to the USA, so I need good old dollar signs.

#2) The way that is the obvious setup is 1 product per page, 3 options per product. We have over 100 products, so I would like to have multiple products on each page. Also, we carry 18 flavors of each type of cig, so we need 18 options, not 3.
*I tried to make each product a seperate blog post on the Products page, but it will only let me add blog entries on the index page.

Any help would be greatly appreciated.

The rest of my questions are about the general layout and how to change it, so I will put that in another section.

*This is my first day working with WP, and eshop, so please be gentle.
Thank you!
                            <br />
                        </p>
                    </div>
                </div>
                </div>
                <jsp:include page="Categories.jsp"/>
                <!--Login table and news-->
                 <jsp:include page="right.jsp"/>
                 <jsp:include page="Footer.jsp"/>
        </div>  
    </body>
</html>
