<%@ page import="p1.Db,java.util.*,p1.Product" %>
<html>
    <head>
        <title>E-Shop Items</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-5">
        <link rel="stylesheet" type="text/css" href="style.css" />
        <%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
    </head>
    <body>

            <jsp:include page="Header.jsp"/>
            <jsp:include page="MenuBar.jsp"/>
            <div id="container">

                <div id ="center" class="column">
                    <div id="content">
                     <%
                         Db obj = new Db();
                         String val =request.getParameter("v1");
                         int min = Integer.parseInt(request.getParameter("v2"));
                         int max = Integer.parseInt(request.getParameter("v3"));
                     %>
                      <h1><%=obj.getCurrentCategory(val)%></h1>
                         <div class="stuff">

                            <%
                                 ArrayList arr = obj.getNameOfRange(val,min,max);

                                for(int i=0;i!=arr.size();i++)
                                {
                                     Product pr= obj.getProduct((String)arr.get(i));
                                     %>
                                     <div class="item">
                                         <img src=<%=pr.getImage()%> alt="" width="124" height="90" />
                                         <a href="Details.jsp?v1=<%=pr.getName()%>" class="name"><%=pr.getName()%></a>
                                         <span>Rs <%=pr.getPrice()%></span>
                                         <a href="Details.jsp?v1=<%=pr.getName()%>"><img src="images/zoom.gif" alt="" width="53" height="19" /></a>
                                         <%
                                                    if(session.getAttribute("login")!=null)
                                                    {
                                                        %>
                                                        <a href="Addcart.jsp?v1=<%=pr.getName()%>&v2=<%=min%>">
                                                        <img src="images/cart.gif" alt="" width="71" height="19" />
                                                         </a>
                                                         <%
                                                    }
                                          %>
                                      </div>
                                      <%
                                 }
                                 obj.close();
                             %>
                         </div>
                    </div>
                    <%
                    if(obj.ismore)
                    {
                      %>
                       <div align="right" >
                            <a href="Items.jsp?v1=<%=val%>&v2=<%=max+1%>&v3=<%=max+11%>"><b><span>next>></span></b></a>
                       </div>
                       <%
                    }
                    %>
                </div>
                <jsp:include page="Categories.jsp"/>
                <!--Login table and news-->
                 <jsp:include page="right.jsp"/>
                 <jsp:include page="Footer.jsp"/>
        </div>
    </body>
</html>
