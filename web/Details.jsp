<%@ page import="p1.Db,p1.Product,java.util.*" %>
<html>        
    <head>
        <title>E-Shop</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-5">
        <link rel="stylesheet" type="text/css" href="style.css" />
    </head>
    <body>
           
            <jsp:include page="Header.jsp"/>
            <jsp:include page="MenuBar.jsp"/>
            <%
                
                Db obj = new Db();
                Product p = obj.getProduct(request.getParameter("v1"));                
            %>
            <div id="container">  
                <div id="center" class="column">
	  	<div id="content">
			<div id="about">                                
				<p class="tree"><a href="Items.jsp?v1=<%=p.getCatid()%>&v2=1&v3=10"><%=obj.getCurrentCategory(p.getCatid())%></a> >><%=p.getName()%></p>
				<div class="photos">
					<img src=<%=p.getImage()%> alt="" width="227" height="215" /><br />
					<a href="#" class="moreph">more photos</a>
					<a href="#" class="comments">View Comments (27)</a>
				</div>
				<div class="description">
					<p><u><%=p.getName()%></u> <span class="price">Rs<%=p.getPrice()%></span></p>
                                        <%
                                            String feat[] = p.getFeatures();
                                        %>
                                        <p><%=feat[0]%></p>
					<p><strong>Short features:</strong></p>
					<ul id="features">
                                            <%
                                                boolean color_changer = true;
                                                for(int k = 1; k< feat.length; k++)
                                                 {
                                                          if(k%2!=0)
                                                        {
                                                          if(color_changer)
                                                          {
                                           %>
                                                              <li class="color">
                                            <%
                                                          }
                                                          else
                                                          {
                                                       %>
                                                              <li>
                                                       <%
                                                          }
                                                          

                                            %>                                            
                                                                <span><%=feat[k]%></span>
                                            <%
                                                         }
                                                         else if(k%2==0)
                                                         {
                                             %>               
                                                            <%=feat[k]%>                                              
                                                             </li>

                                             <%
                                                            color_changer = ! color_changer;
                                                           }
                                              
                                                   }
                                             %>
                                                            
                                            
					</ul>
					<%
                                            if(session.getAttribute("login")!=null)
                                            {
                                            %><a href="Addcart.jsp?v1=<%=p.getName()%>&v2=1"><button>Add to Cart</button></a><img src="images/carts.gif" alt="" width="16" height="24" class="carts" /><%
                                            }
                                         %>
                                           
				</div>
			</div>
			<img src="images/title6.gif" alt="" width="537" height="23" class="pad25" />
			<div class="stuff">
				 <%                                                                                  
                                 ArrayList arr = obj.getSimilarProduct(p.getType());    
                                 
                                for(int i=0;i!=arr.size();i++)
                                {
                                     Product pr= obj.getProduct((String)arr.get(i));                                   
                                     %>
                                     <div class="item">
                                         <img src=<%=pr.getImage()%> alt="" width="124" height="90" /> 
                                         <a href="Details.jsp?v1=<%=pr.getName()%>" class="name"><%=pr.getName()%></a>
                                         <span>Rs <%=pr.getPrice()%></span>
                                         <a href="Details.jsp?v1=<%=pr.getName()%>"><img src="images/zoom.gif" alt="" width="53" height="19" /></a>
                                         <%
                                                    if(session.getAttribute("login")!=null)
                                                    {
                                                        %>
                                                        <a href="Addcart.jsp?v1=<%=pr.getName()%>&v2=1">
                                                        <img src="images/cart.gif" alt="" width="71" height="19" />
                                                         </a>
                                                         <%
                                                    }
                                          %>
                                      </div> 
                                      <%        
                                 }
                                 obj.close();
                             %>                           
			</div>
		</div>
	  </div>               
                <jsp:include page="Categories.jsp"/>
                <!--Login table and news-->
                 <jsp:include page="right.jsp"/>
                 <jsp:include page="Footer.jsp"/>
        </div>  
    </body>
</html>
